import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cost_of_care/repository/inpatient_outpatient_procedure_repository_impl.dart';
import 'package:equatable/equatable.dart';
import '../../models/outpatient_procedure.dart';

part 'inpatient_procedure_event.dart';
part 'inpatient_procedure_state.dart';

class InpatientProcedureBloc
    extends Bloc<InpatientProcedureEvent, InpatientProcedureState> {
  InpatientOutpatientProcedureRepositoryImpl
      inpatientOutpatientProcedureRepositoryImpl;
  InpatientProcedureBloc(this.inpatientOutpatientProcedureRepositoryImpl)
      : super(InpatientProcedureInitial());

  @override
  Stream<InpatientProcedureState> mapEventToState(
    InpatientProcedureEvent event,
  ) async* {
    if (event is FetchData) {
      bool isSaved = false;
      if (!event.isHardRefresh) {
        isSaved =
            inpatientOutpatientProcedureRepositoryImpl.checkSavedInpatient();
      }

      if (isSaved) {
        List<OutpatientProcedure> inpatientList =
            inpatientOutpatientProcedureRepositoryImpl.getSavedInpatient();
        yield InpatientProcedureLoadedState(inpatientList);
      } else {
        try {
          yield InpatientProcedureLoadingState();
          List<OutpatientProcedure> inpatientProcedure =
              await inpatientOutpatientProcedureRepositoryImpl
                  .getInpatientProcedures();
          inpatientOutpatientProcedureRepositoryImpl
              .saveInpatient(inpatientProcedure);
          yield InpatientProcedureLoadedState(inpatientProcedure);
        } catch (e) {
          yield InpatientProcedureErrorState(e.message);
        }
      }
    }
  }
}
