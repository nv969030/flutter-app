import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/bloc.dart';
import 'package:cost_of_care/bloc/report_a_bug_bloc/report_a_bug_bloc.dart';
import 'package:cost_of_care/repository/download_cdm_repository_impl.dart';
import 'package:cost_of_care/screens/download_cdm/download_cdm_screen.dart';
import 'package:cost_of_care/screens/home/home_screen.dart';
import 'package:cost_of_care/screens/report_an_issue/report_an_issue.dart';
import 'package:cost_of_care/screens/saved/saved_screen.dart';
import 'package:cost_of_care/screens/share_app/share_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info/package_info.dart';

class BaseClass extends StatefulWidget {
  @override
  _BaseClassState createState() => _BaseClassState();
}

class _BaseClassState extends State<BaseClass> {
  int selectedIndex = 0;

  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  DownloadFileButtonBloc downloadFileButtonBloc;

  @override
  void dispose() {
    super.dispose();
    downloadFileButtonBloc.close();
  }

  @override
  void initState() {
    super.initState();
    downloadFileButtonBloc =
        new DownloadFileButtonBloc(DownloadCDMRepositoryImpl());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _drawerKey,
      body: IndexedStack(
        index: selectedIndex,
        children: <Widget>[
          Home(_drawerKey),
          DownloadCDMScreen(),
          Saved(downloadFileButtonBloc),
        ],
      ),
      drawer: AppDrawer(),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.cloud_download,
                size: 25,
              ),
              label: 'Download CDM'),
          BottomNavigationBarItem(icon: Icon(Icons.bookmark), label: 'Saved'),
        ],
        currentIndex: selectedIndex,
        fixedColor: Colors.orange,
        onTap: onItemTapped,
      ),
    );
  }

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }
}

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(),
          SizedBox(
            height: 20,
          ),
          _createDrawerItem(
              icon: Icons.description,
              text: 'View CDM Statewise',
              onTap: () => {
                    Navigator.pop(context),
                    Navigator.pushNamed(context, '/ViewCDMStatewise')
                  }),
          // Divider(),
          _createDrawerItem(
              icon: Icons.local_hospital,
              text: 'Compare Hospitals',
              onTap: () => {
                    Navigator.pop(context),
                    Navigator.pushNamed(context, '/CompareHospitals')
                  }),
          // Divider(),
          _createDrawerItem(
              icon: Icons.person,
              text: 'Inpatient Procedures',
              onTap: () => {
                    Navigator.pop(context),
                    Navigator.pushNamed(context, '/IntpatientProcedure')
                  }),
          // Divider(),
          _createDrawerItem(
              icon: Icons.supervised_user_circle,
              text: 'Outpatient Procedures',
              onTap: () => {
                    Navigator.pop(context),
                    Navigator.pushNamed(context, '/OutpatientProcedure')
                  }),
          // Divider(),
          _createDrawerItem(
              icon: Icons.share,
              text: 'Share App',
              onTap: () => {Navigator.pop(context), shareApp()}),
          // Divider(),
          _createDrawerItem(
              icon: Icons.book,
              text: 'About',
              onTap: () => {
                    Navigator.pop(context),
                    Navigator.pushNamed(context, '/About')
                  }),
          // Divider(),
          _createDrawerItem(
              icon: Icons.bug_report,
              text: 'Report A Bug',
              onTap: () async {
                try {
                  await reportABug();
                } catch (e) {
                  BlocProvider.of<ReportABugBloc>(context)
                      .add(ShowErrorSnackBarEvent(e.message));
                }

                Navigator.pop(context);
              }),

          SizedBox(
            height: 10,
          ),
          ListTile(
            title: FutureBuilder(
              future: getAppInfo(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  return Text(
                    "v " + snapshot.data,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  );
                }
                return Text(
                  "v 1.0.0",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                );
              },
            ),
            onTap: () {},
          ),
        ],
      ),
    );
  }

  Future getAppInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String version = packageInfo.version;
    return version;
  }

  Widget _createDrawerItem(
      {IconData icon, String text, GestureTapCallback onTap}) {
    return Padding(
      padding: EdgeInsets.only(left: 8.0),
      child: ListTile(
        title: Text(text),
        leading: Icon(
          icon,
          color: Colors.grey[800],
        ),
        onTap: onTap,
      ),
    );
  }

  Widget _createHeader() {
    return Container(
      height: 200,
      decoration: BoxDecoration(
        color: Colors.orange,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: Align(
              alignment: Alignment.center,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/libre_white.png',
                    height: 200,
                    width: 200,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Text(
              'Providing better cost estimates',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontStyle: FontStyle.italic),
            ),
          ),
          SizedBox(
            height: 8,
          )
        ],
      ),
    );
  }
}
