import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_list/download_cdm_bloc.dart';
import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_list/download_cdm_event.dart';
import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/bloc.dart';
import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/download_file_button_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../main.dart';
import 'components/body.dart';

class DownloadCDMScreen extends StatefulWidget {
  @override
  _DownloadCDMScreenState createState() => _DownloadCDMScreenState();
}

class _DownloadCDMScreenState extends State<DownloadCDMScreen> {
  bool isSearching = false;
  bool isEnabled = true;
  var _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocListener<DownloadFileButtonBloc, DownloadFileButtonState>(
      listener: (BuildContext context, state) async {
        bool value = false;
        if (state is InitialDownloadFileButtonState ||
            state is DownloadButtonErrorState ||
            state is DownloadButtonLoaded) value = true;
        setState(() {
          isEnabled = value;
        });
      },
      child: Scaffold(
        appBar: isSearching
            ? AppBar(
                backgroundColor: Colors.white,
                leading: IconButton(
                    onPressed: () {
                      var state =
                          BlocProvider.of<DownloadFileButtonBloc>(context)
                              .state;
                      if (state is InitialDownloadFileButtonState ||
                          state is DownloadButtonErrorState ||
                          state is DownloadButtonLoaded) {
                        BlocProvider.of<DownloadCdmBloc>(context).add(
                            DownloadCDMSearchHospital('', box.get('state')));
                        setState(() {
                          isSearching = !isSearching;
                        });
                      }
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.black,
                    )),
                title: TextField(
                    autofocus: true,
                    controller: _controller,
                    enabled: isEnabled,
                    style: TextStyle(fontSize: 16),
                    onChanged: (String query) {
                      var state =
                          BlocProvider.of<DownloadFileButtonBloc>(context)
                              .state;
                      if (state is InitialDownloadFileButtonState ||
                          state is DownloadButtonErrorState ||
                          state is DownloadButtonLoaded)
                        BlocProvider.of<DownloadCdmBloc>(context).add(
                            DownloadCDMSearchHospital(
                                query.toLowerCase(), box.get('state')));
                    },
                    decoration: InputDecoration(
                        hintText: 'Search ChargeMaster ...',
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide.none),
                        contentPadding: EdgeInsets.zero,
                        suffixIcon: IconButton(
                          icon: Icon(Icons.clear),
                          color: Colors.black,
                          onPressed: () {
                            var state =
                                BlocProvider.of<DownloadFileButtonBloc>(context)
                                    .state;
                            if (state is InitialDownloadFileButtonState ||
                                state is DownloadButtonErrorState ||
                                state is DownloadButtonLoaded) {
                              _controller.clear();
                              BlocProvider.of<DownloadCdmBloc>(context).add(
                                  DownloadCDMSearchHospital(
                                      '', box.get('state')));
                            }
                          },
                        ))),
              )
            : AppBar(
                backgroundColor: Colors.orange,
                leading: IconButton(
                    icon: Icon(
                      Icons.search,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      var state =
                          BlocProvider.of<DownloadFileButtonBloc>(context)
                              .state;
                      if (state is InitialDownloadFileButtonState ||
                          state is DownloadButtonErrorState ||
                          state is DownloadButtonLoaded) {
                        setState(() {
                          isSearching = !isSearching;
                        });
                      }
                    }),
                title: Text(
                  'Download ChargeMasters',
                  style: TextStyle(color: Colors.white),
                ),
                centerTitle: true,
              ),
        body: Body(box.get('state')),
      ),
    );
  }
}
